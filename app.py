from flask import Flask,render_template
import requests
import json
app = Flask(__name__)

@app.route("/")
def index():
    req = requests.get("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")
    data_id = json.loads(req.content) 
    data = [] 
    for iterator in data_id[:10]:
        req_id= requests.get("https://hacker-news.firebaseio.com/v0/item/"+str(iterator)+".json?print=pretty")
        _d = json.loads(req_id.content)
        data.append(_d)
    return render_template("index.html",data=data)


if __name__=="__main__":
    app.run(debug=True)